provider "aws" {
  region = "us-east-2"
}

resource "aws_key_pair" "example" {
  key_name   = "puppet-key"
  public_key = file("puppet.pub")
}

resource "aws_instance" "example" {
  ami           = "ami-0c7f9161f8491665f"
  instance_type = "t2.micro"
  key_name = aws_key_pair.example.key_name
  tags = {
    Name = "puppet-agent"
  }

  provisioner "remote-exec" {
    inline = [
      "sudo apt-get update",
      "sudo apt-get install -y puppet",
      "sudo puppet apply --modulepath=/etc/puppet/modules ../puppet/manifests/site.pp",
      "sudo yum -y install nginx",
      "sudo systemctl start nginx"
    ]
  }

    connection {
    type        = "ssh"
    user        = "ec2-user"
    private_key = file("puppet")
    host        = self.public_ip
  }

  provisioner "remote-exec" {
    inline = [
      "sudo apt-get update",
      "sudo apt-get install -y puppet",
      "sudo puppet apply --modulepath=/etc/puppet/modules ../puppet/manifests/site.pp",
    ]
  }
}
